@extends('layouts.app')

@section('content')
<div class="card uper">
  <div class="card-header font-weight-bold">
    <span>Upload CSV File</span>
  </div>
  <div class="card-body">
    @if ($errors->any())
    <div class="row justify-content-center">
      <div class="col-md-6 alert alert-danger pb-0">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    </div>
    @endif
    <form action="{{ route('post#import') }}" method="POST" enctype="multipart/form-data">
      @csrf
      <div class="form-group row justify-content-center pt-4">
        <div class="col-md-6 col-8">
          <input type="file" class="form-control" name="uploadFile" id="uploadFile" accept=".csv">
        </div>
      </div>
      <div class="form-group row justify-content-center pt-2">
        <div class="col-md-6 col-8">
          <button class="btn btn-primary">Import File</button>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection
