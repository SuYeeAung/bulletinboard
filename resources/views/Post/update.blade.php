@extends('layouts.app')

@section('content')
<div class="card uper">
  <div class="card-header font-weight-bold">
      <span>Update Post</span>
  </div>
  <div class="card-body">
    <form method="post" action="{{ route('post#updateConfirmation', $post->id) }}" id="update-form">
      @csrf
      @method('PUT')
      <div class="form-group row justify-content-center pt-4">
        <label for="title" class="col-sm-2 col-form-label">Title</label>
        <div class="col-sm-10 col-md-6">
          @if (old('title'))
          <input type="text" class="form-control" value="{{ old('title') }}" name="title" placeholder="title">
          @else
          <input type="text" class="form-control" name="title" value="{{ $post->title }}" placeholder="title">
          @endif
          @if ($errors->has('title'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('title') }}</strong>
          </span>
          @endif
        </div>
      </div>
      <div class="form-group row justify-content-center">
        <label for="description" class="col-sm-2 col-form-label">Description</label>
        <div class="col-sm-10 col-md-6">
          @if (old('description'))
          <textarea class="form-control" rows="3" name="description" placeholder="description">{{ old('description') }}</textarea>
          @else
          <textarea class="form-control" rows="3" name="description" placeholder="description">{{ $post->description }}</textarea>
          @endif
          @if ($errors->has('description'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('description') }}</strong>
          </span>
          @endif
        </div>
      </div>
      <div class="form-group row justify-content-center">
        <label for="status" class="col-sm-2 col-form-label">Status</label>
        <div class="col-sm-10 col-md-6">
          @if (old('description'))
          @if (old('status') === 'on')
          <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="status" name="status" checked>
            <label class="custom-control-label" for="status"></label>
          </div>
          @else
          <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="status" name="status">
            <label class="custom-control-label" for="status"></label>
          </div>
          @endif
          @else
          @if ($post->status === 1)
          <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="status" name="status" checked>
            <label class="custom-control-label" for="status"></label>
          </div>
          @else
          <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="status" name="status">
            <label class="custom-control-label" for="status"></label>
          </div>
          @endif
          @endif
          <!-- <input type="hidden" name="id" value="{{ $post->id }}"> -->
        </div>
      </div>
      <div class="form-group row justify-content-center">
        <label class="col-sm-2"></label>
        <div class="col-sm-10 col-md-6">
          <button type="submit" class="btn btn-primary">Confirm</button>
          <button type="reset" class="btn btn-secondary">Clear</button>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection
