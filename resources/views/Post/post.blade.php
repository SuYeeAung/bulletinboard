@extends('layouts.app')

@section('content')
<div class="card uper">
<div class="card-header font-weight-bold">
    <span>Post List</span>
  </div>

  <div class="card-body">
  <form method="POST" action="{{ route('post#search') }}">
    @csrf
    <div class="form-row mb-4">
        <div class="form-group col-md-5">
          <input type="text" name="title" class="form-control" value="{{ old('title') }}" placeholder="title ,description, user">
        </div>
        <div class="form-group col-md-1">
          <button type="submit" class="btn btn-primary">Search</button>
        </div>
        <div class="form-group col-md-1">
          <a href="{{ route('post#create') }}" class="btn btn-primary">Add</a>
        </div>
        <div class="form-group col-md-1">
          <a href="{{ route('post#getCsv') }}" class="btn btn-primary">Upload</a>
        </div>
        <div class="form-group col-md-1">
          <a href="{{ route('post#export') }}" class="btn btn-primary">Download</a>
        </div>
      </div>


      </div>
          <div class="table-responsive">
            <table class="table table-striped table-hover">
            <thead>
              <tr>
                <th>Post Title</th>
                <th>Post Description</th>
                <th>Posted User</th>
                <th>Posted Date</th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach($postList as $post)
              <tr>
                <td>
                  <a href="#postDetailModal" data-toggle="modal" onclick="viewPostData({{ $post }})">
                    {{ $post->title }}
                  </a>
                </td>
                <td>{{ $post->description }}</td>
                <td>{{ $post->name }}</td>
                <td>{{ $post->updated_at->format('Y/m/d') }}</td>
                <td>
                  <a href="{{ route('post#update', $post->id) }}" class="btn btn-link">
                  Edit
                  </a>
                </td>
                <td>
                  <a href="#postDeleteModal" data-toggle="modal" onclick= "deletePostData({{ $post->id }})" class="btn btn-link" >
                    Delete
                  </a>
                </td>
                <input type="hidden" name="id" value="{{ $post->id }}">
              </tr>
              @endforeach
            </tbody>
          </table>
          {{ $postList->links() }}
          </div>
    </div>

    <div class="modal fade" id="postDeleteModal" tabindex="-1" role="dialog" aria-labelledby="postDeleteModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <form action="" id="deletePostForm" method="post">
          {{ csrf_field() }}
          {{ method_field('DELETE') }}
          <div class="modal-content">
            <div class="modal-header bg-info border-0">
              <h5 class="modal-title font-weight-bold" id="postDeleteModalLabel"></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body text-center font-weight-bold p-4">
              Do you want to Delete?
              @if(!empty($post->id))
              <input type="hidden" class="post_id" name="post_id" value="{{ $post->id }}">
              @endif
            </div>
            <div class="modal-footer border-0 mt-4 justify-content-center">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
              <button type="submit" class="btn btn-danger">Yes</button>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="modal fade" id="postDetailModal" tabindex="-1" role="dialog" aria-labelledby="postDetailModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header bg-info">
            <h5 class="modal-title font-weight-bold" id="postDetailModalLabel">Post Detail</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body justify-content-center">
            <form>
              <div class="form-group row">
                <div class="col-4">
                  <label for="postTitle" class="col-form-label">Title:</label>
                </div>
                <div class="col-8">
                  <textarea class="form-control-plaintext" rows="3" readonly id="postTitle"></textarea>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-4">
                  <label for="postDescription" class="col-form-label">Description:</label>
                </div>
                <div class="col-8">
                  <textarea class="form-control-plaintext" rows="3" readonly id="postDescription"></textarea>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-4">
                  <label for="postStatus" class="col-form-label">Status:</label>
                </div>
                <div class="col-8">
                  <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" disabled id="postStatus">
                    <label class="custom-control-label" for="status"></label>
                  </div>
                </div>
              </div>
            
              <div class="form-group row">
                <div class="col-4">
                  <label for="postCreatedAt" class="col-form-label">Created at:</label>
                </div>
                <div class="col-8">
                  <textarea class="form-control-plaintext" rows="3" readonly id="postCreatedAt"></textarea>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-4">
                  <label for="postCreatedUser" class="col-form-label">Created user:</label>
                </div>
                <div class="col-8">
                  <textarea class="form-control-plaintext" rows="3" readonly id="postCreatedUser"></textarea>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-4">
                  <label for="postLastUpdatedAt" class="col-form-label">Last updated at:</label>
                </div>
                <div class="col-8">
                  <textarea class="form-control-plaintext" rows="3" readonly id="postLastUpdatedAt"></textarea>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-4">
                  <label for="postUpdatedUser" class="col-form-label">Updated user:</label>
                </div>
                <div class="col-8">
                  <textarea class="form-control-plaintext" rows="3" readonly id="postUpdatedUser"></textarea>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </form>
</div>

@endsection
