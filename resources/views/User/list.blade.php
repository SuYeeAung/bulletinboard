@extends('layouts.app')

@section('content')
<div class="card uper">
  <div class="card-header font-weight-bold">
    <i class="fas fa-users"></i>
    <span>User List</span>
  </div>
  <div class="card-body">
    <div class="uper">
      <form method="POST" action="{{ route('user#search') }}">
        {{ csrf_field() }}
        <div class="form-row mb-4">
          <div class="form-group col-md-2">
            <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Name">
          </div>
          <div class="form-group col-md-2">
            <input type="text" name="email" class="form-control" value="{{ old('email') }}" placeholder="Email">
          </div>
          <div class="form-group col-md-2">
            <input type="text" name="from" class="form-control" value="{{ old('from') }}" placeholder="Created From">
          </div>
          <div class="form-group col-md-2">
            <input type="text" name="to" class="form-control" value="{{ old('to') }}" placeholder="Created To">
          </div>
          <div class="form-group col-md-2">
            <button type="submit" class="btn btn-primary">Search</button>
          </div>
          <div class="form-group col-md-2">
            <a href="{{ route('user#create') }}" class="btn btn-primary">Add</a>
          </div>
        </div>
      </form>
      <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
          <thead class="bg-info font-weight-bold text-center">
            <tr>
              <td width="8%">Name</td>
              <td width="10%">Email</td>
              <td width="10%">Created User</td>
              <td width="8%">Phone</td>
              <td width="8%">Birth date</td>
              <td width="15%">Address</td>
              <td width="8%">Created Date</td>
              <td width="8%">Updated Date</td>
              <td width="15%" colspan="2"></td>
            </tr>
          </thead>
          <tbody>
            @foreach($users as $user)
            <tr>
              <td>
                <a href="#userDetailModal" data-toggle="modal" onclick="viewUserData({{ $user }})">
                  {{ $user->name }}
                </a>
              </td>
              <td> {{ $user->email }} </td>
              <td> {{ $user->create_user_name }} </td>
              <td> {{ $user->phone }} </td>
              @if(empty($user->dob))
                <td> {{ $user->dob }} </td>
              @else
                <td> {{ $user->dob->format('Y/m/d') }} </td>
              @endif
              <td> {{ $user->address }} </td>
              <td> {{ $user->created_at->format('Y/m/d') }} </td>
              @if ( ! $user->updated_at )
              <td> {{ $user->updated_at->format('Y/m/d') }} </td>
              @else
              <td> {{ $user->updated_at->format('Y/m/d') }} </td>
              @endif
              <td>
                <a href="userDeleteModal" data-toggle="modal" onclick= "deleteUserData({{ $user }})" class="btn btn-link" >
                  Delete
                </a>
              </td>
              <input type="hidden" name="id" value="{{ $user->id }}">
            </tr>
            @endforeach
          </tbody>
        </table>
        {{ $users->links() }}
      </div>
    </div>
    <div class="modal fade" id="userDeleteModal" tabindex="-1" role="dialog" aria-labelledby="userDeleteModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <form action="" id="deletUserForm" method="post">
          {{ csrf_field() }}
          {{ method_field('DELETE') }}
          <div class="modal-content">
            <div class="modal-header bg-info border-0">
              <h5 class="modal-title font-weight-bold" id="userDeleteModalLabel"></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body text-center font-weight-bold p-4">
              Do you want to delete this user?
            </div>
            <div class="modal-footer border-0 mt-4 justify-content-center">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
              <button type="submit" class="btn btn-danger" >Yes</button>
            </div>
          </div>
        </form>
      </div>
    </div>
    <div class="modal fade" id="userDetailModal" tabindex="-1" role="dialog" aria-labelledby="userDetailModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header bg-info">
            <h5 class="modal-title font-weight-bold" id="userDetailModalLabel">User Detail</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
              <div class="form-group row">
                <div class="col-4">
                  <label for="userNamae" class="col-form-label">Name:</label>
                </div>
                <div class="col-8">
                  <input type="text" class="form-control-plaintext" readonly id="userNamae">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-4">
                  <label for="userEmail" class="col-form-label">Email:</label>
                </div>
                <div class="col-8">
                  <input type="text" class="form-control-plaintext" readonly id="userEmail">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-4">
                  <label for="userPhone" class="col-form-label">Phone No:</label>
                </div>
                <div class="col-8">
                  <input type="text" class="form-control-plaintext" readonly id="userPhone">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-4">
                  <label for="userDob" class="col-form-label">Date of Birth:</label>
                </div>
                <div class="col-8">
                  <input type="text" class="form-control-plaintext" readonly id="userDob">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-4">
                  <label for="userAddress" class="col-form-label">Address:</label>
                </div>
                <div class="col-8">
                  <input type="text" class="form-control-plaintext" readonly id="userAddress">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @endsection
