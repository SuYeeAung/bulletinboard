@extends('layouts.app')

@section('content')
<div class="card uper">
  <div class="card-header font-weight-bold">
    <span>Update User</span>
  </div>
  <div class="card-body">
    <form action="{{ route('user#updateConfirmation', $user->id) }}" method="POST" enctype="multipart/form-data" id="update-form">
      @csrf
      <div class="form-group row justify-content-center">
        <label for="profile" class="col-8 col-form-label"></label>
        <div class="col-4 pl-0 mb-4">
          <img src="{{ $user->profile }}" alt="Image" width="200" height="150" />
          <input type="hidden" class="form-control" id="hiddenProfile" name="hiddenProfile" value="{{ $user->profile }}">
          <input type="hidden" name="id" value="{{ $user->id }}">
        </div>
      </div>
      <div class="form-group row justify-content-center">
        <label for="name" class="col-sm-2 col-form-label">Name<span style="color:red;">*</span></label>
        <div class="col-sm-10 col-md-6">
          @if (old('name'))
          <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name">
          @else
          <input type="text" class="form-control" name="name" value="{{ $user->name }}" placeholder="Name">
          @endif
          @if ($errors->has('name'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('name') }}</strong>
          </span>
          @endif
        </div>
      </div>
      <div class="form-group row justify-content-center">
        <label for="email" class="col-sm-2 col-form-label">Email Address<span style="color:red;">*</span></label>
        <div class="col-sm-10 col-md-6">
          @if (old('email'))
          <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address">
          @else
          <input type="email" class="form-control" name="email" value="{{ $user->email }}" placeholder="Email Address">
          @endif
          @if ($errors->has('email'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('email') }}</strong>
          </span>
          @endif
        </div>
      </div>
      <div class="form-group row justify-content-center">
        <label for="type" class="col-sm-2 col-form-label">Type<span style="color:red;">*</span></label>
        <div class="col-sm-10 col-md-6">
          @if (old('type') != null)
          <select class="form-control" name="type">
            <option>Type</option>
            @if (old('type') == 0)
              <option value="0" selected>Admin</option>
              <option value="1">User</option>
              <option value="2">Visitor</option>
            @elseif (old('type') == 1)
            　<option value="0">Admin</option>
              <option value="1" selected>User</option>
              <option value="2">Visitor</option>
            @else
            　<option value="0">Admin</option>
              <option value="1">User</option>
              <option value="2" selected>Visitor</option>
            @endif
          </select>
          @else
          <select class="form-control" name="type">
            <option>Type</option>
            @if ($user->type == 0)
            <option value="0" selected>Admin</option>
            <option value="1">User</option>
            <option value="2">Visitor</option>
            @elseif ($user->type == 1)
            　<option value="0">Admin</option>
              <option value="1" selected>User</option>
              <option value="2">Visitor</option>
            @else
            　<option value="0">Admin</option>
              <option value="1">User</option>
              <option value="2" selected>Visitor</option>
            @endif
          </select>
          @endif
          @if ($errors->has('type'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('type') }}</strong>
          </span>
          @endif
        </div>
      </div>
      <div class="form-group row justify-content-center">
        <label for="phone" class="col-sm-2 col-form-label">Phone</label>
        <div class="col-sm-10 col-md-6">
          @if (old('phone'))
          <input type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="Phone">
          @else
          <input type="text" class="form-control" name="phone" value="{{ $user->phone }}" placeholder="Phone">
          @endif
          @if ($errors->has('phone'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('phone') }}</strong>
          </span>
          @endif
        </div>
      </div>
      <div class="form-group row justify-content-center justify-content-center">
        <label for="dob" class="col-md-2 col-sm-4 col-form-label">Date of Birth</label>
        <div class="col-md-6 col-sm-6">
          @if (old('dob'))
          <input type="text" class="form-control" name="dob" value="{{ old('dob') }}" placeholder="Date of Birth">
          @else
            @if(empty($user->dob))
                <input type="text" class="form-control" name="dob" value="{{ $user->dob }}" placeholder="Date of Birth">
            @else
                <input type="text" class="form-control" name="dob" value="{{ $user->dob->format('Y/m/d') }}" placeholder="Date of Birth">
            @endif
          @endif
          @if ($errors->has('dob'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('dob') }}</strong>
          </span>
          @endif
        </div>
      </div>
      <div class="form-group row justify-content-center">
        <label for="address" class="col-sm-2 col-form-label">Address</label>
        <div class="col-sm-10 col-md-6">
          @if (old('address'))
          <textarea class="form-control" rows="3" justify-content-centers="3" name="address" placeholder="Address">{{ old('address') }}</textarea>
          @else
          <textarea class="form-control" rows="3" justify-content-centers="3" name="address" placeholder="Address">{{ $user->address }}</textarea>
          @endif
          @if ($errors->has('address'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('address') }}</strong>
          </span>
          @endif
        </div>
      </div>
      <div class="form-group row justify-content-center">
        <label for="address" class="col-md-2 col-sm-4 col-form-label">Profile<span style="color:red;">*</span></label>
        <div class="col-md-6 col-sm-6">
          <input type="file" class="form-control mb-4" id="profile" name="profile" accept=".png,.jpg,jpeg" onchange="loadPreview(this);">
          @if ($errors->has('profile'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('profile') }}</strong>
          </span>
          @endif
          <img id="previewImage" src="" style="display: none;" width="200" height="150" />
        </div>
      </div>
      <div class="form-group row justify-content-center">
        <div class="col-sm-8 col-md-8">
          <a class="btn btn-link" href="{{ route('password#showChangePasswordForm',$user->id) }}">Change Password</a>
        </div>
      </div>
      <div class="form-group row justify-content-center mt-4">
        <label class="col-sm-2"></label>
        <div class="col-sm-10 col-md-6">
          <button type="submit" class="btn btn-primary">Update</button>
          <button type="reset" onClick="clearForm()" class="btn btn-secondary">Cancel</button>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection
