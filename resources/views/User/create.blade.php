@extends('layouts.app')

@section('content')
<div class="card uper">
  <div class="card-header font-weight-bold">
    <span>Create User</span>
  </div>
  <div class="card-body">
    <form action="{{ route('user#confirmation') }}" method="POST" enctype="multipart/form-data" id="create-form" novalidate>
      @csrf
      <div class="form-group row justify-content-center pt-4">
        <label for="name" class="col-md-2 col-sm-4 col-form-label">Name<span style="color:red;">*</span></label>
        <div class="col-md-6 col-sm-6">
          <input type="text" class="form-control" value="{{ old('name') }}" name="name" placeholder="Name">
          @if ($errors->has('name'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('name') }}</strong>
          </span>
          @endif
        </div>
      </div>
      <div class="form-group row justify-content-center">
        <label for="email" class="col-md-2 col-sm-4 col-form-label">Email Address<span style="color:red;">*</span></label>
        <div class="col-md-6 col-sm-6">
          <input type="email" class="form-control" value="{{ old('email') }}" name="email" placeholder="Email Address">
          @if ($errors->has('email'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('email') }}</strong>
          </span>
          @endif
        </div>
      </div>
      <div class="form-group row justify-content-center">
        <label for="password" class="col-md-2 col-sm-4 col-form-label">Password<span style="color:red;">*</span></label>
        <div class="col-md-6 col-sm-6">
          <input type="password" class="form-control" value="{{ old('password') }}" name="password" placeholder="Password">
          @if ($errors->has('password'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('password') }}</strong>
          </span>
          @endif
        </div>
      </div>
      <div class="form-group row justify-content-center">
        <label for="password_confirmation " class="col-md-2 col-sm-4 col-form-label">Confirm Password<span style="color:red;">*</span></label>
        <div class="col-md-6 col-sm-6">
          <input type="password" class="form-control" value="{{ old('password_confirmation') }}" name="password_confirmation" placeholder="Confrim Password">
        </div>
      </div>
      <div class="form-group row justify-content-center">
        <label for="type" class="col-md-2 col-sm-4 col-form-label">Type<span style="color:red;">*</span></label>
        <div class="col-md-6 col-sm-6">
          @if (old('type') != null)
          <select class="form-control" name="type">
            <option>Type</option>
            @if (old('type') == 0)
            <option value="0" selected>Admin</option>
            <option value="1" 　>User</option>
            <option value="2" 　>Visitor</option>
            @elseif (old('type') == 1)
            <option value="0">Admin</option>
            <option value="1" selected>User</option>
            <option value="2">Visitor</option>
            @else
            <option value="0">Admin</option>
            <option value="1">User</option>
            <option value="2" selected>Visitor</option>
            @endif
          </select>
          @else
          <select class="form-control" name="type">
            <option value="">Type</option>
            <option value="0">Admin</option>
            <option value="1">User</option>
            <option value="2">Visitor</option>
          </select>
          @endif
          @if ($errors->has('type'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('type') }}</strong>
          </span>
          @endif
        </div>
      </div>
      <div class="form-group row justify-content-center">
        <label for="phone" class="col-md-2 col-sm-4 col-form-label">Phone</label>
        <div class="col-md-6 col-sm-6">
          <input type="text" class="form-control" value="{{ old('phone') }}" name="phone" placeholder="Phone">
          @if ($errors->has('phone'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('phone') }}</strong>
          </span>
          @endif
        </div>
      </div>
      <div class="form-group row justify-content-center">
        <label for="dob" class="col-md-2 col-sm-4 col-form-label">Date of Birth</label>
        <div class="col-md-6 col-sm-6">
          <input type="text" class="form-control" format="yyyy/mm/dd" value="{{ old('dob') }}" name="dob" placeholder="Date of Birth">
        </div>
      </div>
      <div class="form-group row justify-content-center">
        <label for="address" class="col-md-2 col-sm-4 col-form-label">Address</label>
        <div class="col-md-6 col-sm-6">
          <textarea type="text" class="form-control" rows="3" name="address" placeholder="Address">{{ old('address') }}</textarea>
          @if ($errors->has('address'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('address') }}</strong>
          </span>
          @endif
        </div>
      </div>
      <div class="form-group row justify-content-center">
        <label for="address" class="col-md-2 col-sm-4 col-form-label">Profile<span style="color:red;">*</span></label>
        <div class="col-md-6 col-sm-6">
          <input type="file" class="form-control mb-4" id="profile" name="profile" accept=".png,.jpg,jpeg" onchange="loadPreview(this);">
          <img id="previewImage" src="" style="display: none;" width="200" height="150" />
          @if ($errors->has('profile'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('profile') }}</strong>
          </span>
          @endif
        </div>
      </div>
      <div class="form-group row justify-content-center mt-4">
        <label class="col-md-2 col-sm-4"></label>
        <div class="col-md-6 col-sm-6">
          <button type="submit" class="btn btn-success">Confirm</button>
          <button type="reset" onClick="clearForm()" class="btn btn-secondary">Clear</button>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection
