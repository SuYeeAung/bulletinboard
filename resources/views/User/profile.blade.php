@extends('layouts.app')

@section('content')
<div class="card uper">
  <div class="card-header font-weight-bold">
    <i class="fas fa-user"></i>
    User Profile
  <span>
    <a href="{{ route('user#update', Auth::id()) }}" class="btn btn-link">Edit</a>
  </span>
  </div>
  <div class="card-body">
    <form">
      @csrf
      <div class="form-group row justify-content-md-center justify-content-sm-start">
        <label for="name" class="col-4 col-sm-2 col-form-label ">Name</label>
        <div class="col-8 col-sm-6">
          <input type="text" readonly class="form-control-plaintext" value="{{ $user->name }}" name="name">
        </div>
      </div>
      <div class="form-group row justify-content-right">
        <label for="profile" class="col-4 col-form-label"></label>
        <div class="col-4">
          <img src="{{ $user->profile }}" alt="Image" width="200" height="150" />
        </div>
      </div>
      <div class="form-group row justify-content-md-center justify-content-sm-start">
        <label for="email" class="col-4 col-sm-2 col-form-label">Email Address</label>
        <div class="col-8 col-sm-6">
          <input type="email" readonly class="form-control-plaintext" value="{{ $user->email }}" name="email">
        </div>
      </div>
      <div class="form-group row justify-content-md-center">
        <label for="type" class="col-4 col-sm-2 col-form-label">Type</label>
        <div class="col-8 col-sm-6">
          @if($user->type == 0)
            <input type="text" readonly class="form-control-plaintext" value="Admin" name="type">
          @elseif($user->type == 1)
            <input type="text" readonly class="form-control-plaintext" value="User" name="type">
          @else
            <input type="text" readonly class="form-control-plaintext" value="Visitor" name="type">
          @endif
        </div>
      </div>
      <div class="form-group row justify-content-md-center">
        <label for="phone" class="col-4 col-sm-2 col-form-label">Phone</label>
        <div class="col-8 col-sm-6">
          <input type="text" readonly class="form-control-plaintext" value="{{ $user->phone }}" name="phone">
        </div>
      </div>
      <div class="form-group row justify-content-md-center">
        <label for="dob" class="col-4 col-sm-2 col-form-label">Date of Birth</label>
        <div class="col-8 col-sm-6">
        @if(empty($user->dob))
          <input type="text" readonly class="form-control-plaintext" value="{{ $user->dob }}" name="dob">
        @else
          <input type="text" readonly class="form-control-plaintext" value="{{ $user->dob->format('Y/m/d') }}" name="dob">
        @endif
        </div>
      </div>
      <div class="form-group row justify-content-md-center">
        <label for="address" class="col-4 col-sm-2 col-form-label">Address</label>
        <div class="col-8 col-sm-6">
          <textarea class="form-control-plaintext" readonly name="address" rows="3">{{ $user->address }}</textarea>
        </div>
      </div>
      </form>
  </div>
</div>
@endsection
