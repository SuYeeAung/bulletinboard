<?php

namespace App\Imports;

use App\Models\Post;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ImportPosts implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $duplicateCount = Post::query()
            ->where('title', '=',  $row['title'])
            ->count();
        if ($duplicateCount == 0) {
            return new Post([
                'title' => $row['title'],
                'description' => $row['description'],
                'create_user_id' => "2",
                'updated_user_id' => "2",
                'deleted_user_id' => "2",
                'created_at' => date('Y-m-d H:i:s'),
                'deleted_at' => date('Y-m-d H:i:s')
            ]);
        }
        dd("End ImportPosts");
    }
}
