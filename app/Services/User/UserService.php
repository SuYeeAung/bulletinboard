<?php

namespace App\Services\User;

use App\Contracts\Dao\User\UserDaoInterface;
use App\Contracts\Services\User\UserServiceInterface;
use App\Dao\User\UserDao;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class UserService implements UserServiceInterface
{
    private $userDao;

  /**
   * Class Constructor
   * @param OperatorUserDaoInterface
   * @return
   */
  public function __construct(UserDaoInterface $userDao)
  {
    $this->userDao = $userDao;
  }

  /**
   * Get User List
   * @param Object
   * @return $userList
   */
  public function getUserList()
  {
    return $this->userDao->getUserList();
  }

      /**
     * search
     *
     * @param Request $request
     * @return obj [OR] null
     */
    public function searchUserList($request)
    {
        $result = $this->userDao->searchUserList($request["name"], $request["email"], $request["from"], $request["to"]);
        return $result;
    }

    /**
     * store user
     *
     * @param Request $request
     * @return obj [OR] null
     */
    public function storeUser($request)
    {
        $request["password"] = bcrypt($request["password"]);
        $request["dob"] = Carbon::createFromFormat('Y/m/d', $request["dob"])->format('Y-m-d');
        $request["profile"] = $request["profile"];
        $request["create_user_id"] = Auth::id();
        $request["updated_user_id"] = Auth::id();
        $request["deleted_user_id"] = Auth::id();
        $request["created_at"] = date('Y-m-d H:i:s');
        $request["updated_at"] = date('Y-m-d H:i:s');
        $request["deleted_at"] = date('Y-m-d H:i:s');
        $result = $this->userDao->storeUser($request);
        return $result;
    }

    /**
     * get user by id
     *
     * @param Request $id
     * @return obj
     */
    public function getUserById($id)
    {
        $result = $this->userDao->getUserById($id);
        return $result;
    }

    /**
     * update user
     *
     * @param Request $request, $id
     * @return obj [OR] null
     */
    public function updateUser($request, $id)
    {
        $request["updated_user_id"] = Auth::id();
        $request["updated_at"] = date('Y-m-d H:i:s');
        $result = $this->userDao->updateUser($request, $id);
        return $result;
    }

    /**
     * delete user
     *
     * @param $id
     * @return void
     */
    public function deleteUser($id)
    {
        $this->userDao->deleteUser($id);
    }
}
