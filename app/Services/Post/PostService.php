<?php

namespace App\Services\Post;

use App\Contracts\Dao\Post\PostDaoInterface;
use App\Contracts\Services\Post\PostServiceInterface;
use App\Dao\Post\PostDao;
use Illuminate\Support\Facades\Auth;

class PostService implements PostServiceInterface
{
    /**
     * Private variable $postDao
     *
     */
    private $postDao;
    /**
     * Constructor
     *
     * @param PostDaoInterface $postDao
     */
    public function __construct(PostDaoInterface $postDao)
    {
        $this->postDao = $postDao;
    }

    /**
     * get post list
     *
     * @param Request $request
     * @return obj [OR] null
     */
    public function getPostList()
    {
        $result = $this->postDao->getPostList();
        return $result;
    }

    /**
     * search
     *
     * @param Request $request
     * @return obj [OR] null
     */
    public function searchPostList($title)
    {
        $result = $this->postDao->searchPostList($title);
        return $result;
    }

    /**,
     * store post
     *
     * @param Request $request
     * @return obj [OR] null
     */
    public function storePost($request)
    {
        $request["create_user_id"] = Auth::id();
        $request["updated_user_id"] = Auth::id();
        $request["deleted_user_id"] = Auth::id();
        $request["create_at"] = date('Y-m-d H:i:s');
        $request["updated_at"] = date('Y-m-d H:i:s');
        $request["deleted_at"] = date('Y-m-d H:i:s');
        $result = $this->postDao->storePost($request);
        return $result;
    }

    /**
     * get post by id
     *
     * @param Request $id
     * @return obj
     */
    public function getPostById($id)
    {
        $result = $this->postDao->getPostById($id);
        return $result;
    }

    /**
     * update post
     *
     * @param Request $request, $id
     * @return obj [OR] null
     */
    public function updatePost($request, $id)
    {
        $request["updated_user_id"] = Auth::id();
        $request["updated_at"] = date('Y-m-d H:i:s');
        $result = $this->postDao->updatePost($request, $id);
        return $result;
    }

    /**
     * delete post
     *
     * @param $id
     * @return void
     */
    public function deletePost($id)
    {
        $this->postDao->deletePost($id);
    }
}
