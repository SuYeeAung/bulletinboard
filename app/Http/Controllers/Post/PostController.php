<?php

namespace App\Http\Controllers\Post;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Services\Post\PostService;
use App\Contracts\Services\Post\PostServiceInterface;
use App\Imports\ImportPosts;
use App\Exports\ExportPosts;
use Maatwebsite\Excel\Facades\Excel;
use \phpoffice\phpspreadsheet\src\PhpSpreadsheet\Reader\Csv;
use Illuminate\Support\Facades\Session;

use Log;

class PostController extends Controller
{
    /**
     * Private variable $postService
     */
    private $postService;

    /**
     * Create a new controller instance.
     *
     * @param PostServiceInterface $postService
     */
    public function __construct(PostServiceInterface $postService)
    {
        $this->postService = $postService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // 全件取得とページネーション
        $postList = $this->postService->getPostList();
        return view('post.post', compact('postList'));
    }

    /**
     * Show post creating view
     *
     * @return void
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Validation and go to post confirmation view
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function confirmation(Request $request)
    {
        $validator = $this->validateInputForm($request);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $data['post'] = $request;
        Session::put('POST_INPUT_DATA', $request->all());
        return view('post.confirm', $data);
    }

        /**
     * Validate post input form request
     *
     * @param Request $request
     * @return void
     */
    private function validateInputForm(Request $request)
    {
        $rules = [
            'title' => 'required|max:255|unique:posts',
            'description' => 'required|max:255',
        ];
        return Validator::make($request->all(), $rules);
    }

    /**
     * back to post create page with old input
     *
     * @return void
     */
    public function backPostInput()
    {
        $oldInputData = Session::get('POST_INPUT_DATA');
        Session::forget('POST_INPUT_DATA');
        return redirect('/create')->withInput($oldInputData);
    }


    /**
     * Store post resources
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = $this->postService->storePost($request->except('_token'));
        return redirect('/post')->with('success', 'successful');
    }

    /**
     * Show post updating view
     *
     * @param  int  $id
     * @return void
     */
    public function show($id)
    {
        $post = $this->postService->getPostById($id);
        return view('post.update', compact('post'));
    }

    /**
     * Show post update confirmation view
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return void
     */
    public function updateConfirmation(Request $request)
    {
        $validator = $this->validateUpdateForm($request);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $data['post'] = $request;
        return view('post.updateConfirm', $data);
    }

    /**
     * Validate post update form request
     *
     * @param Request $request
     * @return void
     */
    private function validateUpdateForm(Request $request)
    {
        $rules = [
            'title' => 'required|max:255|unique:posts,title,' . $request->id,
            'description' => 'required|max:255',
        ];
        return Validator::make($request->all(), $rules);
    }

    /**
     * Update post resources
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->postService->updatePost($request->except('_method', '_token', 'id'), $id);
        return redirect('/post')->with('success', 'Update Successful!');
    }

    /**
     * Delete post resources
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $this->postService->deletePost($request->post_id);
        return redirect('/post')->with('success', 'Delete Successful');
    }

    /**
     * Display a listing of post.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $postList = $this->postService->searchPostList($request["title"]);
        $postList->appends(request()->all())->render();
        return view('post.post', compact("postList"));
    }

    /**
     * Show post csv importing view
     *
     * @param
     * @return
     */
    public function getCsv()
    {
        
        return view('post.upload');
    }

        /**
     * Import post file
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function import(Request $request)
    {
        $validator_one = $this->validateImportFile($request);
        if ($validator_one->fails()) {
            return redirect()->back()->withInput()->withErrors($validator_one);
        }
        $rows = Excel::toArray(new ImportPosts, $request->file('uploadFile'));
        foreach ($rows as $row) {
            $validator_two = Validator::make($row, $this->rules(), $this->validationMessages());
            if ($validator_two->fails()) {
                foreach ($validator_two->errors()->messages() as $messages) {
                    foreach ($messages as $error) {
                        return redirect()->back()->withInput()->withErrors($error);
                    }
                }
            }
        }
        Excel::import(new ImportPosts, request()->file('uploadFile'));
        return redirect('/post')->with('success', 'Successful Upload File');
    }

    /**
     * Validate import file request
     *
     * @param Request $request
     * @return void
     */
    private function validateImportFile(Request $request)
    {
        $rules = [
            'uploadFile' => 'required|mimes:csv,txt',
        ];
        return Validator::make($request->all(), $rules);
    }

    
    /**
     * Validate post import file
     *
     * @return void
     */
    private function rules(): array
    {
        return [
            '*.title' => 'required|max:255|unique:posts',
            '*.description' => 'required|max:255',
        ];
    }

    /**
     * Download post list
     *
     * @return \Illuminate\Support\Collection
     */
    public function export()
    {
        return Excel::download(new ExportPosts, 'posts.xlsx');
    }

    /**
     * Get custom validation error message
     *
     * @return string
     */
    private function validationMessages()
    {
        return [
            '*.title.required' => trans('The file requires a title.'),
            '*.title.max' => trans('The title cannot exceed 255 characters.'),
            '*.title.unique' => trans('The title already exists.'),
            '*.description.required' => trans('The file requires a description.'),
            '*.description.max' => trans('The description cannot exceed 255 characters.'),
        ];
    }

}
