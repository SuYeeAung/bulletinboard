<?php

namespace App\Http\Controllers\User;

use App\Contracts\Services\User\UserServiceInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Log;

class UserController extends Controller
{
    private $userService;

    /**
     * Create a new controller instance.
     *
     * @param UserServiceInterface $userService
     */
    public function __construct(UserServiceInterface $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Display a listing of user.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->userService->getUserList();
        return view('user.list', compact('users'));
    }

    /**
     * Display a listing of user.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $this->checkRequest($request);
        $users = $this->userService->searchUserList($request->except('_token'));
        $users->appends(request()->all())->render();
        return view('user.list', compact('users'));
    }

    /**
     * Delete user resources
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $this->userService->deleteUser($id);
        return redirect('/user')->with('success', 'Successuful Delete');
    }

    /**
     * Display user profile view
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile($id)
    {
        $user = $this->userService->getUserById($id);
        return view('user.profile', compact('user'));
    }

    /**
     * Show user creating view
     *
     * @param  int
     * @return
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Validation and go to user confirmation form
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function confirmation(Request $request)
    {
        $validator = $this->validateInputForm($request);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        if ($request->file('profile')) {
            $userId = $request->id;
            $isExit = File::exists(public_path() . "/images/$userId");
            if (!$isExit) {
                Storage::makeDirectory(public_path() . "/images/$userId");
            }
            $imageName = $request->profile->getClientOriginalName();
            $request->profile->move(public_path("images/$userId"), $imageName);
        }
        $data['user'] = $request;
        Session::put('IMAGE', $imageName);
        Session::put('ID', $userId);
        Session::put('USER_INPUT_DATA', $request->except('profile'));
        return view('user.confirm', $data);
    }

    /**
     * Show user update confirmation view
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateConfirmation(Request $request)
    {
        $validator = $this->validateUpdateForm($request);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        if ($request->file('profile')) {
            $userId = Auth::id();
            $isExit = File::exists(public_path() . "/images/$userId");
            if (!$isExit) {
                Storage::makeDirectory(public_path() . "/images/$userId");
            }
            $imageName = $request->profile->getClientOriginalName();
            $request->profile->move(public_path("images/$userId"), $imageName);
        }
        $data['user'] = $request;
        Session::put('USER_UPDATE_DATA', $request->except('profile'));
        return view('user.updateConfirm', $data);
    }

    /**
     * Update user resources
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return
     */
    public function update(Request $request, $id)
    {
        $this->userService->updateUser($request->except('_token'), $id);
        return redirect('/user/user')->with('success', 'Update Successful.');
    }

    /**
     * Validate user input form request
     *
     * @param Request $request
     * @return void
     */
    private function validateInputForm(Request $request)
    {
        $rules = [
            'name' => 'required|max:50|unique:users',
            'email' => 'required|email|max:50|unique:users',
            'phone' => 'required|numeric|regex:/(0)[0-9]/|digits_between:8,11',
            'password' => 'required|min:8|max:25|confirmed|regex:/^(?=.*[A-Z])(?=.*\d).+$/',
            'profile' => 'required|image|mimes:jpeg,jpg,png|max:1024',
            'type' => 'required',
            'dob' => 'required|after:01/01/1940|before:01/01/2011|date_format:Y/m/d',
            'address' => 'max:255',
        ];
        return Validator::make($request->all(), $rules);
    }

    /**
     * Validate user update form request
     *
     * @param Request $request
     * @return void
     */
    private function validateUpdateForm(Request $request)
    {
        $rules = [
            'name' => 'required|max:50|unique:users,name,' . $request->id,
            'email' => 'required|email|max:50|unique:users,email,' . $request->id,
            'phone' => 'required|numeric|regex:/(0)[0-9]/|digits_between:8,11',
            'profile' => 'image|mimes:jpeg,jpg,png|max:1024',
            'type' => 'required',
            'dob' => 'required|after:01/01/1940|before:01/01/2011|date_format:Y/m/d',
            'address' => 'max:255',
        ];
        return Validator::make($request->all(), $rules);
    }

    /**
     * Check Request key is missing
     *
     * @param Request $request
     * @return void
     */
    private function checkRequest($request)
    {
        if ($request->missing('name')) {
            $request["name"] = null;
        }
        if ($request->missing('email')) {
            $request["email"] = null;
        }
        if ($request->missing('from')) {
            $request["from"] = null;
        }
        if ($request->missing('to')) {
            $request["to"] = null;
        }
    }

        /**
     * Store user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->userService->storeUser($request->except('_token'));
        return redirect('/user/user')->with('success', 'Create Successful');
    }

    /**
     * Show user update view
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->userService->getUserById($id);
        return view('user.update', compact('user'));
    }

    /**
     * back to user update page with old input
     *
     * @return void
     */
    public function backUserUpdate()
    {
        $oldUpdateData = Session::get('USER_UPDATE_DATA');
        Session::forget('USER_UPDATE_DATA');
        $returnRoute = '/user/' . $oldUpdateData["id"];
        return redirect($returnRoute)->withInput($oldUpdateData);
    }

}
