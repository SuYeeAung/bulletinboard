<?php

namespace App\Http\Controllers\Auth;

use App\Contracts\Services\Auth\LoginServiceInterface;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Validator;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Private variable $loginService
     */
    private $loginService;

    /**
     * Create a new controller instance.
     *
     * @param LoginServiceInterface $loginService
     */
    public function __construct(LoginServiceInterface $loginService)
    {
        $this->loginService = $loginService;
    }


    /**
     * Show change password form
     *
     * @return \Illuminate\Http\Response
     */
    public function showChangePasswordForm()
    {
        return view('auth.passwords.change');
    }

    /**
     * Change password input form request
     *
     * @param Request $request
     * @return void
     */
    public function change(Request $request)
    {
        $validator = $this->validateInputForm($request);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $request->merge(['password' => bcrypt($request->new_password)]);
        $this->loginService->changePassword($request);
        return redirect('/user/user')->with('success', 'Update Successful.');
    }

    /**
     * Validate change password input form request
     *
     * @param Request $request
     * @return void
     */
    private function validateInputForm(Request $request)
    {
        $rules = [
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => 'required|min:8|regex:/^(?=.*[A-Z])(?=.*\d).+$/',
            'password_confirmation' => ['same:new_password'],
        ];
        return Validator::make($request->all(), $rules);
    }
}
