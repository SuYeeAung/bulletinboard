<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Home画面
Route::get('/home', 'HomeController@index')->name('home');

// Posts画面
Route::get('/post', 'Post\PostController@index')->name('post');
Route::any('/search', 'Post\PostController@search')->name('post#search');
Route::get('/create', 'Post\PostController@create')->name('post#create');
Route::post('/confirm', 'Post\PostController@confirmation')->name('post#confirmation');
Route::post('/', 'Post\PostController@store')->name('post#store');
Route::put('/{post}', 'Post\PostController@update')->name('post#update');
Route::get('/{id}', 'Post\PostController@show')->name('post#show');
Route::any('/{id}/updateConfirmation', 'Post\PostController@updateConfirmation')->name('post#updateConfirmation');
Route::delete('/{id}', 'Post\PostController@delete')->name('post#delete');
Route::get('post/importView', 'Post\PostController@getCsv')->name('post#getCsv');
Route::post('post/import', 'Post\PostController@import')->name('post#import');
Route::get('post/export', 'Post\PostController@export')->name('post#export');
Route::get('post/backInput', 'Post\PostController@backPostInput')->name('post#backPostInput');


// Users画面
Route::get('user/user', 'User\UserController@index')->name('user');
Route::any('user/search', 'User\UserController@search')->name('user#search');
Route::get('user/create', 'User\UserController@create')->name('user#create');
Route::post('user/confirm', 'User\UserController@confirmation')->name('user#confirmation');
Route::post('user/', 'User\UserController@store')->name('user#store');
Route::delete('user/{id}', 'User\UserController@delete')->name('user#delete');
Route::get('user/profile/{id}', 'User\UserController@profile')->name('user#profile');
Route::get('user/{id}', 'User\UserController@show')->name('user#show');
Route::post('user/{id}', 'User\UserController@update')->name('user#update');
Route::any('user/{id}/updateConfirmation', 'User\UserController@updateConfirmation')->name('user#updateConfirmation');
Route::get('user/backUpdate', 'User\UserController@backUserUpdate')->name('user#backUserUpdate');

// User Profile画面
Route::post('/change', 'Auth\ResetPasswordController@change')->name('password#change');
Route::get('/change/{id}', 'Auth\ResetPasswordController@showChangePasswordForm')->name('password#showChangePasswordForm');
