const { defaultsDeep } = require("lodash");

// post delete data preparation
function deletePostData(id) {
  $(".post_id").attr('value', id);
  $("#deletePostForm").attr('action', '/post/' + id);
}

  // view post data
  function viewPostData(post) {
    $("#postTitle").val(post.title);
    $("#postDescription").html(post.description);
    if (post.status === 1) {
      document.getElementById("postStatus").checked = true;
    } else {
      document.getElementById("postStatus").checked = false;
    }
    var postCreatedAt = getFormattedDate(post.created_at);
    $("#postCreatedAt").val(postCreatedAt);
    $("#postCreatedUser").val(post.name);
    var postLastUpdatedAt = getFormattedDate(post.updated_at);
    $("#postLastUpdatedAt").val(postLastUpdatedAt);
    $("#postUpdatedUser").val(post.name);
  }

    // view user data
    function viewUserData(user) {
      var userDob = "";
      if (user.dob != null) {
        userDob = getFormattedDate(user.dob);
      }
      $("#userNamae").val(user.name);
      $("#userEmail").val(user.email);
      $("#userAddress").val(user.address);
      $("#userDob").val(userDob);
      $("#userPhone").val(user.phone);
    }

    // user delete data preparation
    function deleteUserData(user) {
        var url = '{{ route("user#delete", ":id") }}';
        url = url.replace(':id', user.id);
        $("#deletUserForm").attr('action', url);
        $("#userName").html(user.name);
    }

      // format date
  function getFormattedDate(input) {
    var datePart = input.match(/\d+/g),
      year = datePart[0],
      month = datePart[1],
      day = datePart[2];
    return year + '/' + month + '/' + day;
  }

    // load user profile preview
    function loadPreview(input, id) {
      id = id || '#previewImage';
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $(id)
            .attr('src', e.target.result)
            .width(200)
            .height(150);
          $(id).css("display", "block");
        };
        reader.readAsDataURL(input.files[0]);
      }
    }

  // reset data
  function clearForm() {
    $('#previewImage').attr('src', "");
    $('#previewImage').css("display", "none");
  }

