<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('posts')->insert([
            'id' => '001',
            'title' => 'Post_title_01',
            'description' => 'Post_Description01',
            'status' => '1',
            'create_user_id' => '001',
            'updated_user_id' => '001',
            'deleted_user_id' => '001',
            'created_at' => '2020-07-07',
            'updated_at' => '2020-07-07',
            'deleted_at' => '2020-07-07',
        ]);
        DB::table('posts')->insert([
            'id' => '002',
            'title' => 'Post_title_02',
            'description' => 'Post_Description02',
            'status' => '1',
            'create_user_id' => '001',
            'updated_user_id' => '001',
            'deleted_user_id' => '001',
            'created_at' => '2020-07-07',
            'updated_at' => '2020-07-07',
            'deleted_at' => '2020-07-07',
        ]);
        DB::table('posts')->insert([
            'id' => '003',
            'title' => 'Post_title_03',
            'description' => 'Post_Description03',
            'status' => '1',
            'create_user_id' => '001',
            'updated_user_id' => '001',
            'deleted_user_id' => '001',
            'created_at' => '2020-07-14',
            'updated_at' => '2020-07-14',
            'deleted_at' => '2020-07-14',
        ]);

    }
}
