<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'id' => '001',
            'name' => 'User001',
            'email' => 'user001@gmail.com',
            'password' => bcrypt('user0001'),
            'profile' => 'profile255_User001',
            'type' => '1',
            'phone' => '09999',
            'address' => 'botahtaung',
            'dob' => '2020-01-01',
            'create_user_id' => '001',
            'updated_user_id' => '001',
            'deleted_user_id' => '001',
            'created_at' => '2020-07-07',
            'updated_at' => '2020-07-07',
            'deleted_at' => '2020-07-07',
        ]);
    }
}
